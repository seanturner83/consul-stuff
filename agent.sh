#!/bin/bash

if [ "FOO${1}T" == 'FOOT' ]; then echo "Usage: $0 <nodename>"; exit 1; fi
if [[ -z "${CONSUL_MASTER_TOKEN}" ]]; then echo "Master token not set: please set CONSUL_MASTER_TOKEN"; exit 1; fi

attempt_num=0

until curl -is --max-time 2 --header "X-Consul-Token: ${CONSUL_MASTER_TOKEN}" http://127.0.0.1:8500/v1/acl/list | head -n1 | grep -q '200 OK'
do
  if (( attempt_num >= 10 ))
  then
    exit 1
  else
    sleep 1
    ((attempt_num++))
  fi
done

if curl -s --header "X-Consul-Token: ${CONSUL_MASTER_TOKEN}" http://127.0.0.1:8500/v1/acl/list | grep -q "$1";
then
  sleep 1
else

  tokenjson=$(curl -s \
    --request PUT \
    --header "X-Consul-Token: foo" \
    --data \
  '{
    "Name": "'$1' Token",
    "Type": "client",
    "Rules": "node \"'$1'\" { policy = \"write\" } service \"\" { policy = \"read\" }"
  }' http://127.0.0.1:8500/v1/acl/create)

  token=$(echo $tokenjson | jq '.ID')

#  echo $token

  curl -s \
    --request PUT \
    --header "X-Consul-Token: ${CONSUL_MASTER_TOKEN}" \
    --data \
  "{
    \"Token\": ${token}
  }" http://127.0.0.1:8500/v1/agent/token/acl_agent_token
fi
