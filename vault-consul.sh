#!/bin/bash

if [[ -z "${CONSUL_MASTER_TOKEN}" ]]; then echo "Master token not set: please set CONSUL_MASTER_TOKEN"; exit 1; fi

attempt_num=0

until curl -is --max-time 2 --header "X-Consul-Token: ${CONSUL_MASTER_TOKEN}" http://127.0.0.1:8500/v1/acl/list | head -n1 | grep -q '200 OK'
do
  if (( attempt_num >= 10 ))
  then
    exit 1
  else
    sleep 1
    ((attempt_num++))
  fi
done

if curl -s --header "X-Consul-Token: ${CONSUL_MASTER_TOKEN}" http://127.0.0.1:8500/v1/acl/list | grep -q 'Vault Token';
then

  tokenjson=$(curl http://127.0.0.1:8500/v1/acl/list -s --header "X-Consul-Token: ${CONSUL_MASTER_TOKEN}" | jq -r '.[] | select(.Name | contains("Vault Token")) | {"ID"}')

else

  tokenjson=$(curl -s \
    --request PUT \
    --header "X-Consul-Token: foo" \
    http://127.0.0.1:8500/v1/acl/create \
    -d @- <<EOF
{
    "Name": "Vault Token",
    "Type": "client",
    "Rules": "
{
  \"key\": {
    \"vault/\": {
      \"policy\": \"write\"
    }
  },
  \"node\": {
    \"\": {
      \"policy\": \"write\"
    }
  },
  \"service\": {
    \"vault\": {
      \"policy\": \"write\"
    }
  },
  \"agent\": {
    \"\": {
      \"policy\": \"write\"
    }

  },
  \"session\": {
    \"\": {
      \"policy\": \"write\"
    }
  }
}
"
}
EOF
  )
fi

token=$(echo $tokenjson | jq -r '.ID')

echo $token

sed -i "s/VAULTTOKENREPLACEME/${token}/g" /vault/config/vault.hcl